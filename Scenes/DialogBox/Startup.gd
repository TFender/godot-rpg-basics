extends NinePatchRect

onready var window_width = get_viewport().get_visible_rect().size.x;
onready var window_length = get_viewport().get_visible_rect().size.y;
onready var animation_player = $AnimationPlayer;
onready var text_container = $TextContainer;

var dialog_rect_size = Vector2.ZERO;
var offset_percentage = 0.05;
var grow = "Grow";
var shrink = "Shrink";

func _ready():
	visible = false;
	var lengthOffset = window_length * offset_percentage;
	var startX = lengthOffset;
	var width = window_width - (2 * lengthOffset)
	var startY = (window_length - (window_length / 3.5));
	var length = window_length - lengthOffset - startY;
	rect_position = Vector2(startX, startY);
	dialog_rect_size = Vector2(width, length);	
	text_container.setup(dialog_rect_size);
	start_animations();
	
func start_animations():
	set_grow_animation();
	set_shrink_animation();
	yield(get_tree().create_timer(3.0), "timeout");
	play_grow_animation();
	
func start_text_container():
	text_container.loop_over_lines();

func set_grow_animation():
	var animation = animation_player.get_animation(grow);
	animation.track_set_key_value(0, 1, dialog_rect_size);	
	
func set_shrink_animation():
	var animation = animation_player.get_animation(shrink);
	animation.track_set_key_value(0, 0, dialog_rect_size);	

func play_grow_animation():
	animation_player.play(grow);
	
func play_shrink_animation():
	animation_player.play(shrink);

func set_dialog_invisible():
	visible = false;
	queue_free();
	
func set_dialog_visible():
	visible = true;

func _on_TextContainer_dialog_is_done():
	play_shrink_animation();
