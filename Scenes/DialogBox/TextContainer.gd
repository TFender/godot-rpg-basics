extends Control

const letter_scene = preload("res://Scenes/Letter/Letter.tscn")
const blinker_scene = preload("res://Scenes/Blinker/Blinker.tscn")
signal dialog_is_done;

var dialog = "Hello everyone, this is a dialog box. It is meant to show some dialog, that characters can have with eachother, or to show some other information. After x amount of lines, we want to see the blinking sign, which tells us to go to the next part of the dialog. Do you understand?!"

var bigger_offset = 15;
var small_offset = 10;
var x = 8;
var y = 16;
var scale = 2;
var max_chars_on_line: int = 0;
var max_lines_in_box = 3;
var wait_for_input;
var wait_for_completion = false;

var blinker: Node; 
var letters = []; 
var chunks = [];
var lines = [];

func setup(size):
	adjust_x_and_y_to_scale();
	set_textbox_size(size);
	set_textbox_position();
	determine_max_chars();
	create_lines();

func create_letter(letter, x_index, y_index):
	var letter_instance = letter_scene.instance();
	add_letter(letter_instance);
	var new_pos = Vector2(x * x_index, y * y_index); 
	letter_instance.rect_size *= scale;
	letter_instance.rect_scale *= scale;
	letter_instance.rect_position = new_pos;
	letter_instance.set_letter(letter);
	
func add_letter(letter_instance):
	letters.push_front(letter_instance)
	self.add_child(letter_instance)

func adjust_x_and_y_to_scale():
	x *= scale;
	y *= scale;

func set_textbox_size(size):
	size.x -= bigger_offset + 5;
	size.y -= bigger_offset;
	rect_size = size;
	
func set_textbox_position():
	rect_position.x += bigger_offset + 2;
	rect_position.y += small_offset;
	
func determine_max_chars():
	max_chars_on_line = rect_size.x / x;
	
func create_lines():
	var index = 0;
	var line_index = 1;
	var line = "";
	for n in range(dialog.length()):
		var character = dialog[n];
		
		if index == 0 and character == " ":
			continue;
			
		index += 1;
		line += character;
		if n == dialog.length() - 1 or index == max_chars_on_line or (index == max_chars_on_line - 5 and line_index == max_lines_in_box):
			lines.push_back(line)
			index = 0;
			line = ""; 
			if line_index == max_lines_in_box:
				line_index = 1;
			else:
				line_index += 1;
	
func loop_over_lines():
	var index = 0;
	for i in range(max_lines_in_box):
		if !lines.empty():
			wait_for_completion = true;
			var line = lines[0];
			loop_over_dialog_text(line, i);
			lines.remove(0);
			while wait_for_completion:
				yield(get_tree().create_timer(0.1), "timeout");
	show_blinker();
		
func show_blinker():
	yield(get_tree().create_timer(0.3), "timeout");
	wait_for_input = true;
	blinker = blinker_scene.instance();
	self.add_child(blinker);
	var offset = 5
	var new_pos = Vector2(rect_size.x - 2, rect_size.y)
	blinker.position = new_pos;

			
func _process(delta):
	if wait_for_input and Input.is_action_pressed("ui_accept"):
		wait_for_input = false;
		blinker.queue_free();
		clear_letters();
		if lines.empty():
			signal_done();
		else:
			loop_over_lines();

func clear_letters():
	for letter in letters:
		letter.queue_free();
	letters.clear();

func loop_over_dialog_text(text, line_height):	
	var index = 0;
	for letter in str(text):
		var x_index: int = index % max_chars_on_line;
		create_letter(letter,x_index, line_height);
		yield(get_tree().create_timer(0.02), "timeout");
		index+=1;
	wait_for_completion = false;

func signal_done():
	emit_signal("dialog_is_done");
