extends Control

const alpfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+"abcdefghijklmnopqrstuvwxyz"+"1234567890.,?!_-\"'       "+"ÀÁÂÄÈÉÊËÌÍÎÏÙÚÛÜÒÓÔÖÑÇ    "+"àáâäèéêëìíîïùúûüòóôöñç    ";

onready var image = $font_6_16;
onready var animation_player = $AnimationPlayer;

var use_light_up = true;

func _ready():
	if (use_light_up):
		animation_player.play("LightUp");

var wrong_index = 70;
var space = 71;

func set_letter(letter):
	var index = space;
	if (letter != " "):
		index = alpfabet.find(letter)
	if (index == -1):
		index = wrong_index;
	image.frame = index;

func disable_light_up():
	use_light_up = false;
